#include <WiFi.h>

#include "WemoSwitch.h"
#include "WemoManager.h"
#include "CallbackFunction.h"

// prototypes
boolean connectWifi();

//on/off callbacks
void powerOn();
void powerOff();
void wowOn();
void wowOff();
void txOn();
void txOff();
void opsOn();
void opsOff();
void mxOn();
void mxOff();

//------- Replace the following! ------
char ssid[] = "JOES2 6760";       // your network SSID (name)
char password[] = "imaguest!";  // your network key

WemoManager wemoManager;
WemoSwitch *power = NULL;
WemoSwitch *wow = NULL;
WemoSwitch *tx = NULL;
WemoSwitch *ops = NULL;
WemoSwitch *mx = NULL;

const int ledPin = 13;
const int ledPin1 = 26;
const int ledPin2 = 27;
const int ledPin3 = 25;
const int ledPin4 = 33;

void setup()
{
  Serial.begin(115200);

  // Set WiFi to station mode and disconnect from an AP if it was Previously
  // connected
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);

  // Attempt to connect to Wifi network:
  Serial.print("Connecting Wifi: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  IPAddress ip = WiFi.localIP();
  Serial.println(ip);

  wemoManager.begin();
  // Format: Alexa invocation name, local port no, on callback, off callback
  power = new WemoSwitch("power", 80, powerOn, powerOff);
  wow = new WemoSwitch("wow", 81, wowOn, wowOff);
  tx = new WemoSwitch("tx", 82, txOn, txOff);
  ops = new WemoSwitch("ops", 83, opsOn, opsOff);
  mx = new WemoSwitch("mx", 84, mxOn, mxOff);
 
  wemoManager.addDevice(*power);
  wemoManager.addDevice(*wow);
  wemoManager.addDevice(*tx);
  wemoManager.addDevice(*ops);
  wemoManager.addDevice(*mx);
  
  pinMode(ledPin, OUTPUT); // initialize digital ledPin as an output.
  pinMode(ledPin1, OUTPUT); // initialize digital ledPin1 as an output.
  pinMode(ledPin2, OUTPUT); // initialize digital ledPin2 as an output.
  pinMode(ledPin3, OUTPUT); // initialize digital ledPin3 as an output.
  pinMode(ledPin4, OUTPUT); // initialize digital ledPin4 as an output.
  delay(10);
  digitalWrite(ledPin, HIGH); // power is active Low, so high is off
  digitalWrite(ledPin1, HIGH); // wow is active Low, so high is off
  digitalWrite(ledPin2, HIGH); // tx is active Low, so high is off
  digitalWrite(ledPin3, HIGH); // ops is active Low, so high is off
  digitalWrite(ledPin4, HIGH); // mx is active Low, so high is off
}

void loop()
{
  wemoManager.serverLoop();
}

void powerOn() {
    Serial.print("power turn on ...");
    digitalWrite(ledPin, LOW);
}

void powerOff() {
    Serial.print("power turn off ...");
    digitalWrite(ledPin, HIGH);
}
void wowOn() {
    Serial.print("wow turn on ...");
    digitalWrite(ledPin1, LOW);
}

void wowOff() {
    Serial.print("wow turn off ...");
    digitalWrite(ledPin1, HIGH);
}
void txOn() {
    Serial.print("tx turn on ...");
    digitalWrite(ledPin2, LOW);
}

void txOff() {
    Serial.print("tx turn off ...");
    digitalWrite(ledPin2, HIGH);
}
void opsOn() {
    Serial.print("ops turn on ...");
    digitalWrite(ledPin3, LOW);
}

void opsOff() {
    Serial.print("ops turn off ...");
    digitalWrite(ledPin3, HIGH);
}
void mxOn() {
    Serial.print("mx turn on ...");
    digitalWrite(ledPin4, LOW);
}

void mxOff() {
    Serial.print("mx turn off ...");
    digitalWrite(ledPin4, HIGH);
}
