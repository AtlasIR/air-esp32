﻿<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        * {
            box-sizing: border-box;
        }

        /* Create two equal columns that floats next to each other */
        .column {
            float: left;
            width: 50%;
            padding: 10px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }
        
        
h1 {
  color: #1188F2;
  padding: 1vh;
  display: block;
  font-size: 3em;
  margin-top: 0.17em;
  margin-bottom: 0.17em;
  margin-left: 0;
  margin-right: 0;
  font-weight: bold;
}
h2 {
  color: #070707;
  padding: 1vh;
  display: block;
  font-size: 2em;
  margin-top: 0.17em;
  margin-bottom: 0.17em;
  margin-left: 0;
  margin-right: 0;
  font-weight: bold;
}
h3 {
  color: #070707;
  padding: 1vh;
  display: block;
  font-size: 1em;
  margin-top: 0.17em;
  margin-bottom: 0.17em;
  margin-left: 0;
  margin-right: 0;
  font-weight: bold;
}
button {
  display: inline-block;
  background-color: #28B463;
  border: none;
  border-radius: 1px;
  color: white;
  padding: 5px 10px;
  text-decoration: none;
  font-size: 15px;
  margin: 2px;
  
}
button2 {
  display: inline-block;
  background-color: #F23D11;
  border: none;
  border-radius: 1px;
  color: white;
  padding: 5px 15px;
  text-decoration: none;
  font-size: 20px;
  margin: 2px;
  
}

    </style>
</head>
<body>
<div class="row">
    <h1>Scarito Box Discrete Tester</h1>
	<h3><em>UNIT POWER</em><h3>		
                <p><a href="/13/on"><button class=\"button\" style="text-decoration: none;">ON</a></p>
                <p><a href="/13/off"><button class=\"button button2\" style="background-color:#F23D11;">OFF</button></a></p>
	</div>
<div class="row">
        <div class="column" style="background-color:#FDFEFE;">
                <h2>Discrete Test</h2>
        <p>
            <i class="fas fa-thermometer-half" style="color:#059e8a;"</i> 
            <span class="dht-labels" style="font-size: 30px;"><em>Temperature Zone 1</em></span> 
            <span id="temperature">%TEMPZ1%</span>
            <sup class="units">°C</sup>
	</p>
	<p>
            <i class="fas fa-thermometer-half" style="color:#059e8a;"</i> 
            <span class="dht-labels" style="font-size: 30px;"><em>Temperature Zone 2</em></span>
            <span id="temperature">%TEMPZ2%</span>
            <sup class="units">°C</sup>
        </p>	
<BR>
                <h3><em>WOW</em></h3>		
                <p><a href="/26/on"><button class=\"button\" style="text-decoration: none;">ON</a></p>
                <p><a href="/26/off"><button class=\"button button2\" style="background-color:#F23D11;">OFF</button></a></p>
                <h3><em>TXE</em></h3>	
                <p><a href="/27/on"><button class=\"button\" style="background-color:#28B463; margin:5px;">ON</a></p>
                <p><a href="/27/off"><button class=\"button button2\" style="background-color:#F23D11;">OFF</button></a></p>
                <h3><em>OPS</em></h3>
                <p><a href="/25/on"><button class=\"button\" style="background-color:#28B463; margin:5px;">ON</a></p>
                <p><a href="/25/off"><button class=\"button button2\" style="background-color:#F23D11;">OFF</button></a></p>
                <h3><em>MXE</em></h3>
                <p><a href="/33/on"><button class=\"button\" style="background-color:#28B463; margin:5px;">ON</a></p>
                <p><a href="/33/off"><button class=\"button button2\" style="background-color:#F23D11;">OFF</button></a></p>
        </div>
        <div class="column" style="background-color:#FDFEFE;">
            <h2>Discrete Test 2</h2>
	<p>
            <i class="fas fa-thermometer-half" style="color:#059e8a;"</i> 
            <span class="dht-labels" style="font-size: 30px;"><em>Temperature Zone 3</em></span> 
            <span id="temperature">%TEMPZ3%</span>
            <sup class="units">°C</sup>
	</p>
	<p>
            <i class="fas fa-thermometer-half" style="color:#059e8a;"</i> 
            <span class="dht-labels" style="font-size: 30px;"><em>Temperature Zone 4</em></span>
            <span id="temperature">%TEMPZ4%</span>
            <sup class="units">°C</sup>
        </p>
<BR>	
            <h3><em>POWER DISABLE</em></h3>
            <p><a href="/21/on"><button class=\"button\" style="background-color:#28B463; margin:5px;">ON</a></p>
            <p><a href="/21/off"><button class=\"button button2\" style="background-color:#F23D11;">OFF</button></a></p>
            <h3><em>FACTORY DEFAULT RESET</em></h3>
            <p><a href="/22/on"><button class=\"button\" style="background-color:#28B463; margin:5px;">ON</a></p>
            <p><a href="/22/off"><button class=\"button button2\" style="background-color:#F23D11;">OFF</button></a></p>
            <h3><em>POWER FAIL ALERT</em></h3>
            <p><a href="/23/on"><button class=\"button\" style="background-color:#28B463; margin:5px;">ON</a></p>
            <p><a href="/23/off"><button class=\"button button2\" style="background-color:#F23D11;">OFF</button></a></p>
            <h3><em>MODEM RESET</em></h3>
            <p><a href="/32/on"><button class=\"button\" style="background-color:#28B463; margin:5px;">ON</a></p>
            <p><a href="/32/off"><button class=\"button button2\" style="background-color:#F23D11;">OFF</button></a></p>
        </div>
     </div>

</body>
</html>
