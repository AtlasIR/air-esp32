#include <AsyncTCP.h>

/*********
Rui Santos
Complete project details at https://randomnerdtutorials.com
*********/
// Import required libraries
#include "WiFi.h"
#include "ESPAsyncWebServer.h"
#include "SPIFFS.h"
// debug log, set to 1 to enable
#define ENABLE_DEBUG_LOG 0
// Replace with your network credentials
const char* ssid = "Discrete Tester";
const char* password = "123456789";
// Set Discrete GPIO
const int output26 = 26;
const int output27 = 27;
const int output25 = 25;
const int output33 = 33;
const int output21 = 21;
const int output22 = 22;
const int output23 = 23;
const int output32 = 32;
const int output13 = 13;
 int TEMPZ1 = 000;
 int TEMPZ2 = 000;
// Stores Discrete state
String output26State = "off";
String output27State = "off";
String output25State = "off";
String output33State = "off";
String output21State = "off";
String output22State = "off";
String output23State = "off";
String output32State = "off";
String output13State = "off";
//String ledState;
// Create AsyncWebServer object on port 80
AsyncWebServer server(80);
// Replaces placeholder with LED state value
String processor(const String& var){
Serial.println(var);

if(var == "STATE"){
if (digitalRead(output26)){
output26State = "ON";
}
else{
output26State = "OFF";
}
Serial.print(output26State);
return output26State;
if (digitalRead(output27)){
output27State = "ON";
}
else{
output27State = "OFF";
}
Serial.print(output27State);
return output27State;
if (digitalRead(output25)){
output25State = "ON";
}
else{
output25State = "OFF";
}
Serial.print(output25State);
return output25State;
}
if (digitalRead(output33)){
output33State = "ON";
}
else{
output33State = "OFF";
}
Serial.print(output33State);
return output33State;
if (digitalRead(output21)){
output21State = "ON";
}
else{
output21State = "OFF";
}
Serial.print(output21State);
return output21State;
if (digitalRead(output22)){
output22State = "ON";
}
else{
output22State = "OFF";
}
Serial.print(output22State);
return output22State;
if (digitalRead(output23)){
output23State = "ON";
}
else{
output23State = "OFF";
}
Serial.print(output23State);
return output23State;
if (digitalRead(output32)){
output32State = "ON";
}
else{
output32State = "OFF";
}
Serial.print(output32State);
return output32State;
if (digitalRead(output13)){
output13State = "ON";
}
else{
output13State = "OFF";
}
Serial.print(output13State);
return output13State;
return String();
}

void setup(){
// Serial port for debugging purposes
Serial.begin(115200);

// Initialize the output variables as outputs
pinMode(output26, OUTPUT);
pinMode(output27, OUTPUT);
pinMode(output25, OUTPUT);
pinMode(output33, OUTPUT);
pinMode(output21, OUTPUT);
pinMode(output22, OUTPUT);
pinMode(output23, OUTPUT);
pinMode(output32, OUTPUT);
pinMode(output13, OUTPUT);
// Set outputs to High
digitalWrite(output26, HIGH);
digitalWrite(output27, HIGH);
digitalWrite(output25, HIGH);
digitalWrite(output33, HIGH);
digitalWrite(output21, HIGH);
digitalWrite(output22, HIGH);
digitalWrite(output23, HIGH);
digitalWrite(output32, HIGH);
digitalWrite(output13, HIGH);
// Initialize SPIFFS
if(!SPIFFS.begin(true)){
Serial.println("An Error has occurred while mounting SPIFFS");
return;
}
Serial.print("Setting AP (Access Point)…");
// Remove the password parameter, if you want the AP (Access Point) to be open
WiFi.softAP(ssid, password);
IPAddress IP = WiFi.softAPIP();
Serial.print("AP IP address: ");
Serial.println(IP);
server.begin();
// // Connect to Wi-Fi
// WiFi.begin(ssid, password);
// while (WiFi.status() != WL_CONNECTED) {
// delay(1000);
// Serial.println("Connecting to WiFi..");
// }
//
// // Print ESP32 Local IP Address
// Serial.println(WiFi.localIP());
// Route for root / web page
server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to load style.css file
server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
request->send(SPIFFS, "/style.css", "text/css");
});
// Route to set GPIO to HIGH
server.on("/26/on", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output26, LOW);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to LOW
server.on("/26/off", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output26, HIGH);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to HIGH
server.on("/27/on", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output27, LOW);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to LOW
server.on("/27/off", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output27, HIGH);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to HIGH
server.on("/25/on", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output25, LOW);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to LOW
server.on("/25/off", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output25, HIGH);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to HIGH
server.on("/33/on", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output33, LOW);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to LOW
server.on("/33/off", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output33, HIGH);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to HIGH
server.on("/21/on", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output21, LOW);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to LOW
server.on("/21/off", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output21, HIGH);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to HIGH
server.on("/22/on", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output22, LOW);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to LOW
server.on("/22/off", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output22, HIGH);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to HIGH
server.on("/23/on", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output23, LOW);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to LOW
server.on("/23/off", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output23, HIGH);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to HIGH
server.on("/32/on", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output32, LOW);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to LOW
server.on("/32/off", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output32, HIGH);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to HIGH
server.on("/13/on", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output13, LOW);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to LOW
server.on("/13/off", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output13, HIGH);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// rs-server config
const int serverPort = 1234;
  
// rs port config
const int baudrate = 115200;
const int rs_config = SERIAL_8N1;
  
// reading buffor config
#define BUFFER_SIZE 1024
  
// global objects
WiFiServer server;
byte buff[BUFFER_SIZE];
  
void debug_log(char* str) {
#if ENABLE_DEBUG_LOG == 1
   Serial.println(str);
#endif

// Start server
server.begin();
}
void loop() {
// wait for client
   WiFiClient client = server.available();
if (!client)
return;
  
   debug_log("client found");
while (client.connected()) {
int size = 0;
  
// read data from wifi client and send to serial
while ((size = client.available())) {
           size = (size >= BUFFER_SIZE ? BUFFER_SIZE : size);
           client.read(buff, size);
           Serial.write(buff, size);
           Serial.flush();
}
  
// read data from serial and send to wifi client
while ((size = Serial.available())) {
           size = (size >= BUFFER_SIZE ? BUFFER_SIZE : size);
           Serial.readBytes(buff, size);
           client.write(buff, size);
           client.flush();
}
}
   debug_log("client disconnected");
   client.stop();
}
  
