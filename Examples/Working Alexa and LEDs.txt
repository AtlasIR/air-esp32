#include <Arduino.h>
#ifdef ESP32
    #include <WiFi.h>
    #include "ESPAsyncWebServer.h"
#else
    #include <ESP8266WiFi.h>
#endif
#include "fauxmoESP.h"

// Rename the credentials.sample.h file to credentials.h and 
// edit it according to your router configuration
#include "credentials.h"

fauxmoESP fauxmo;

// -----------------------------------------------------------------------------

#define SERIAL_BAUDRATE     115200

#define output26            26
#define output27            27
#define output25            25
#define output33            33
#define output13            13
#define output21            21
#define output22            22
#define output23            23
#define output32            32

#define ID_output26         "wow"
#define ID_output27         "tx"
#define ID_output25         "ops"
#define ID_output33         "mx"
#define ID_output13         "power"
#define ID_output21         "ac fail"
#define ID_output22         "power enable"
#define ID_output23         "diag reset"
#define ID_output32         "transmit mute"

// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Wifi
// -----------------------------------------------------------------------------

void wifiSetup() {

    // Set WIFI module to STA mode
    WiFi.mode(WIFI_STA);

    // Connect
    Serial.printf("[WIFI] Connecting to %s ", WIFI_SSID);
    WiFi.begin(WIFI_SSID, WIFI_PASS);

    // Wait
    while (WiFi.status() != WL_CONNECTED) {
        Serial.print(".");
        delay(100);
    }
    Serial.println();

    // Connected!

}

void setup() {

    // Init serial port and clean garbage
    Serial.begin(SERIAL_BAUDRATE);
    Serial.println();
    Serial.println();

    // LEDs
    pinMode(output26, OUTPUT);
    pinMode(output27, OUTPUT);
    pinMode(output25, OUTPUT);
    pinMode(output33, OUTPUT);
    pinMode(output13, OUTPUT);
    pinMode(output21, OUTPUT);
    pinMode(output22, OUTPUT);
    pinMode(output23, OUTPUT);
    pinMode(output32, OUTPUT);   
    digitalWrite(output26, HIGH);
    digitalWrite(output27, HIGH);
    digitalWrite(output25, HIGH);
    digitalWrite(output33, HIGH);
    digitalWrite(output13, HIGH);
    digitalWrite(output21, HIGH);
    digitalWrite(output22, HIGH);
    digitalWrite(output23, HIGH);
    digitalWrite(output32, HIGH);
    

    // Wifi
    wifiSetup();

    // By default, fauxmoESP creates it's own webserver on the defined port
    // The TCP port must be 80 for gen3 devices (default is 1901)
    // This has to be done before the call to enable()
    fauxmo.createServer(true); // not needed, this is the default value
    fauxmo.setPort(80); // This is required for gen3 devices

    // You have to call enable(true) once you have a WiFi connection
    // You can enable or disable the library at any moment
    // Disabling it will prevent the devices from being discovered and switched
    fauxmo.enable(true);

    // You can use different ways to invoke alexa to modify the devices state:
    // "Alexa, turn yellow lamp on"
    // "Alexa, turn on yellow lamp
    // "Alexa, set yellow lamp to fifty" (50 means 50% of brightness, note, this example does not use this functionality)

    // Add virtual devices
    fauxmo.addDevice(ID_output26);
    fauxmo.addDevice(ID_output27);
    fauxmo.addDevice(ID_output25);
    fauxmo.addDevice(ID_output33);
    fauxmo.addDevice(ID_output13);
    fauxmo.addDevice(ID_output21);
    fauxmo.addDevice(ID_output22);
    fauxmo.addDevice(ID_output23);
    fauxmo.addDevice(ID_output32);

    fauxmo.onSetState([](unsigned char device_id, const char * device_name, bool state, unsigned char value) {
        
        // Callback when a command from Alexa is received. 
        // You can use device_id or device_name to choose the element to perform an action onto (relay, LED,...)
        // State is a boolean (ON/OFF) and value a number from 0 to 255 (if you say "set kitchen light to 50%" you will receive a 128 here).
        // Just remember not to delay too much here, this is a callback, exit as soon as possible.
        // If you have to do something more involved here set a flag and process it in your main loop.
        
        Serial.printf("[MAIN] Device #%d (%s) state: %s value: %d\n", device_id, device_name, state ? "ON" : "OFF", value);

        // Checking for device_id is simpler if you are certain about the order they are loaded and it does not change.
        // Otherwise comparing the device_name is safer.

        if (strcmp(device_name, ID_output26)==0) {
            digitalWrite(output26, state ? LOW : HIGH);
        } else if (strcmp(device_name, ID_output27)==0) {
            digitalWrite(output27, state ? LOW : HIGH);
        } else if (strcmp(device_name, ID_output25)==0) {
            digitalWrite(output25, state ? LOW : HIGH);
        } else if (strcmp(device_name, ID_output33)==0) {
            digitalWrite(output33, state ? LOW : HIGH);
        } else if (strcmp(device_name, ID_output13)==0) {
            digitalWrite(output13, state ? LOW : HIGH);
        } else if (strcmp(device_name, ID_output21)==0) {
            digitalWrite(output13, state ? LOW : HIGH);
        }else if (strcmp(device_name, ID_output22)==0) {
            digitalWrite(output13, state ? LOW : HIGH);
        }else if (strcmp(device_name, ID_output23)==0) {
            digitalWrite(output13, state ? LOW : HIGH);
        }else if (strcmp(device_name, ID_output32)==0) {
            digitalWrite(output13, state ? LOW : HIGH);
        }
        
    });

}

void loop() {

    // fauxmoESP uses an async TCP server but a sync UDP server
    // Therefore, we have to manually poll for UDP packets
    fauxmo.handle();

    // This is a sample code to output free heap every 5 seconds
    // This is a cheap way to detect memory leaks
    static unsigned long last = millis();
    if (millis() - last > 5000) {
        last = millis();
        Serial.printf("[MAIN] Free heap: %d bytes\n", ESP.getFreeHeap());
    }

    // If your device state is changed by any other means (MQTT, physical button,...)
    // you can instruct the library to report the new state to Alexa on next request:
    // fauxmo.setState(ID_YELLOW, true, 255);

}