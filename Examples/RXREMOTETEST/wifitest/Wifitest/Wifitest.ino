/*
   -- New project --
   
   This source code of graphical user interface 
   has been generated automatically by RemoteXY editor.
   To compile this code using RemoteXY library 2.4.3 or later version 
   download by link http://remotexy.com/en/library/
   To connect using RemoteXY mobile app by link http://remotexy.com/en/download/                   
     - for ANDROID 4.5.1 or later version;
     - for iOS 1.4.1 or later version;
    
   This source code is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.    
*/

//////////////////////////////////////////////
//        RemoteXY include library          //
//////////////////////////////////////////////

// RemoteXY select connection mode and include library 
#ifdef REMOTEXY_MODE__ESP32CORE_WIFI

#include <WiFi.h>
// RemoteXY connection settings 
#define REMOTEXY_WIFI_SSID "JOES2 6760"
#define REMOTEXY_WIFI_PASSWORD "imaguest!"
#define REMOTEXY_SERVER_PORT 80

#else

#define REMOTEXY_MODE__ESP32CORE_BLE
#define REMOTEXY_BLUETOOTH_NAME "RemoteXY"
#endif

#include <RemoteXY.h>



// RemoteXY configurate  
#pragma pack(push, 1)
uint8_t RemoteXY_CONF[] =
  { 255,5,0,0,0,210,0,10,254,2,
  2,1,33,11,22,11,8,8,22,11,
  2,26,31,31,79,78,0,79,70,70,
  0,2,1,9,26,22,11,8,21,22,
  11,2,26,31,31,79,78,0,79,70,
  70,0,129,0,56,14,23,6,32,10,
  18,6,17,80,79,87,69,82,0,129,
  0,32,29,16,6,32,23,18,6,17,
  87,79,87,0,2,1,9,40,22,11,
  9,34,22,11,2,26,31,31,79,78,
  0,79,70,70,0,2,1,52,26,22,
  11,9,48,22,11,2,26,31,31,79,
  78,0,79,70,70,0,129,0,32,42,
  8,6,33,36,18,6,17,84,88,0,
  129,0,76,29,13,6,34,50,18,6,
  17,79,80,83,0,2,1,52,40,22,
  11,53,33,22,11,2,26,31,31,79,
  78,0,79,70,70,0,129,0,77,43,
  9,6,53,36,18,6,17,77,88,0,
  129,0,22,1,56,6,37,1,18,6,
  17,68,73,83,67,82,69,84,69,32,
  84,69,83,84,69,82,0 };
  
// this structure defines all the variables and events of your control interface 
struct {

    // input variables
  uint8_t POWER; // =1 if switch ON and =0 if OFF 
  uint8_t WOW; // =1 if switch ON and =0 if OFF 
  uint8_t TX; // =1 if switch ON and =0 if OFF 
  uint8_t OPS; // =1 if switch ON and =0 if OFF 
  uint8_t MX; // =1 if switch ON and =0 if OFF 

    // other variable
  uint8_t connect_flag;  // =1 if wire connected, else =0 

} RemoteXY;
#pragma pack(pop)

/////////////////////////////////////////////
//           END RemoteXY include          //
/////////////////////////////////////////////

#define PIN_POWER 13
#define PIN_WOW 26
#define PIN_TX 27
#define PIN_OPS 25
#define PIN_MX 33


void setup() 
{
  RemoteXY_Init (); 
  
  pinMode (PIN_POWER, OUTPUT);
  pinMode (PIN_WOW, OUTPUT);
  pinMode (PIN_TX, OUTPUT);
  pinMode (PIN_OPS, OUTPUT);
  pinMode (PIN_MX, OUTPUT);
  
  // TODO you setup code
  
}

void loop() 
{ 
  RemoteXY_Handler ();
  
  digitalWrite(PIN_POWER, (RemoteXY.POWER==0)?HIGH:LOW);
  digitalWrite(PIN_WOW, (RemoteXY.WOW==0)?HIGH:LOW);
  digitalWrite(PIN_TX, (RemoteXY.TX==0)?HIGH:LOW);
  digitalWrite(PIN_OPS, (RemoteXY.OPS==0)?HIGH:LOW);
  digitalWrite(PIN_MX, (RemoteXY.MX==0)?HIGH:LOW);
  
  // TODO you loop code
  // use the RemoteXY structure for data transfer
  // do not call delay() 


}
