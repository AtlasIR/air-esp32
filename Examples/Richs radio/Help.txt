Rick's Net Radio
================

A simple and useful net/web radio with useful features and no bull.

For this 'sketch', you will need:
  * Arduino IDE with ESP32 extensions and two additional libraries
  
  * An ESP32 module
  * A VS1053 audio decoder module
  * A 1602 LCD module with IIC backpack
  * Two buttons
  * Some wire to hook everything together

Details and instructions on my blog:
  https://www.heyrick.co.uk/blog/index.php?diary=20190203


Rick.
