﻿// Rick's little internet radio
//   For the ESP32. This is a no-bull version. It will connect, play a station
//   (supporting redirection), and handle displaying metadata on an LCD for
//   title/artist.
//   Version 0.04  1st February 2019
//   http://www.heyrick.co.uk/blog/index.php?diary=20190203
//   Licenced under the EUPL (v1.1 only).



// Include some standard libraries
#include <WiFi.h>                   // WiFi support
#include <HTTPClient.h>             // Fetch stuff from the internet
#include <esp_wifi.h>               // ESP32 specific WiFi functions
#include <Wire.h>                   // IIC


// Now include libraries to work with the hardware we're using
//   VS1053 is from https://github.com/baldram/ESP_VS1053_Library
//     download it and install it from zip file
//   LiquidCrystal_PCF8574 is available in the library manager
//     it's the one by Matthias Hertel
#include <VS1053.h>                 // VS0153 based MP3 decoder board
#include <LiquidCrystal_PCF8574.h>  // IIC connected 1602 LCD

// We're using the standard SPI pins, but we will need to define the
// extra pins for CS, DCS, and DREQ.
#define VS1053_CS    32 
#define VS1053_DCS   33  
#define VS1053_DREQ  35 

// The volume level goes from 0 (off) to 100 (max).
// This volume provides a reasonable level to give me  music without
// disturbing others when plugged into speakers beside my monitor.
// (I'll add volume control buttons later on)
#define INITVOLUME   82

// The 1602 LCD is controlled by a PCF8574 with a default base address of &27
#define IICADDR      0x27

// Buttons
//    Button           Short press    Long press
//      + (next)       Vol+           Next station
//      - (prev)       Vol-           Previous station
#define NEXTBUTTON   13
#define PREVBUTTON   12

#define ACTIONNONE   0
#define ACTIONPRESS  1
#define ACTIONLONGPRESS 2


// Now hardwire the AP's SSID and password.
char ssid[] = "JOES2 6760"; 
char pass[] = "imaguest!";


// Project ID (must be <16 chars for LCD)
char appname[] = "Rick's NetRadio\xBC"; // &BC is "shi" in Katakana, like a little smiley :)
char hostname[] = "RicksNetRadio";      // must be UNIQUE if you have more than one radio...

// Define radio stations
typedef struct stationdef
{
   char name[64];         // Textual name of station ("Eagle '80s")
   char host[128];        // Hostname ("streaming.ukrd.com")
   char path[128];        // Path to complete URI ("/eagle-80s.mp3")
   int port;              // Port number (80)
}

struct stationdef station[5] =
{
   // First station - this is the station played at startup
   "Eagle '80s",
   "streaming.ukrd.com",  // may be updated if there's a redirection
   "/eagle-80s.mp3",      // may be updated if there's a redirection
   80,

   // Second station
   "Eagle 96.4FM",
   "streaming.ukrd.com",
   "/eagle.mp3",
   80,

   // Third station
   "BBC Radio 4 FM",
   "bbcmedia.ic.llnwd.net",
   "/stream/bbcmedia_radio4fm_mf_p",
   80,

   // Fourth station
   "Alouette",
   "broadcast.infomaniak.net",
   "/alouette-high.mp3",
   80,

   //// Fifth station
   //"RTE1",      <-- whatever this does on connect, it crashes the ESP!
   //"av.rasset.ie",
   //"/av/live/radio/adio1.m3u",
   //80,

   // Fifth station
   "JPopsuki Radio!",
   "jpopsuki.fm",
   "/stream",
   8000

   // Don't forget to update stationcount below
}

struct buttondef
{
   bool state;
   unsigned long press;
   unsigned long release;
   int  action;
} 
char button[2];


// The globals
int  lcdpresent = 0;      // Set to '1' if LCD is detected
int  connected = 0;       // Set to '1' if connected to a station
int  bitrate = 0;         // The stream bitrate (in kbit)
int  metaint = 0;         // The interval of when metadata appears (usually 8192-32768)
int  bytecount = 0;       // The number of bytes to go until there's a metadata block
char metadata[4080];      // ICY metadata - global for speed
int  currentstation = 0;  // Currently selected station
int  stationcount = 5;    // How many stations we have defined
int  volume = INITVOLUME; // Current volume
char title[17] = "";      // Current song title
char artist[17] = "";     // Current song artist
int  wantmetadata = 1;    // Do we want metadata?
unsigned long lcddelay=0; // Delay for temporary messages on LCD (0=none, also is a timeout)


// Define an instance of the 16x2 LCD
LiquidCrystal_PCF8574 lcd(IICADDR);

// Define an instance of the VS1053, and give it an aligned 32 byte buffer
VS1053 audioplayer(VS1053_CS, VS1053_DCS, VS1053_DREQ);
__attribute__((aligned(4))) uint8_t buffer[32]; // word aligned for speed

// Define an instance of the WiFi client
WiFiClient webclient;



// Setup code
void setup ()
{
   char serinfo[64];
   
   // Set up serial port for tracing activity (debugging)
   Serial.begin(115200);
   Serial.print(appname);
   Serial.println(" starting up.");
   sprintf(serinfo, "Running on CPU %d at %dMHz, with %d memory free.", xPortGetCoreID(), ESP.getCpuFreqMHz(), ESP.getFreeHeap());
   Serial.println(serinfo);
   
   // Is an LCD connected?
   Serial.print("Looking for LCD...");
   Wire.begin();
   Wire.beginTransmission(IICADDR);
   if ( Wire.endTransmission() == 0 )
   {
      Serial.println("found.");
      lcdpresent = 1;

      // Initialise the LCD (we can't assume anything about its state)
      lcd.begin(16, 2);
      lcd.setBacklight(127);
      lcd.home();
      lcd.clear();
      lcd.noBlink();
      lcd.noCursor();
      lcd.setCursor(0, 0);
      lcd.print(appname);
      lcd.setCursor(0, 1);
      lcd.print("initialising...");
   }
   else
   {
     Serial.println("not detected (is IICADDR correct?).");
   }

   // Set up the blue LED (will light when connected to a station)
   pinMode(2, OUTPUT);
   digitalWrite(2, LOW); // default to off

   // Set up the previous/next buttons
   pinMode(NEXTBUTTON, INPUT_PULLUP);  // pulled up, so ground
   pinMode(PREVBUTTON, INPUT_PULLUP);  // to activate the button
   button[0].state = HIGH;
   button[1].state = HIGH;
   button[0].action = ACTIONNONE;
   button[1].action = ACTIONNONE;

   // Set up SPI
   SPI.begin();

   // Initialise the MP3 decoder - this takes time
   mp3_initialise();

   // Now connect to the AP
   wifi_connect();

   // Done, we'll resume in loop...
}


void loop()
{
   // Round and round we go...
   //
   // Warning: Adding more to this loop will probably require
   //          some sort of buffering to be implemented...

   uint8_t bytes = 0;
   int  reply = 0;

   // Connect?
   if ( !connected )
   {
      // Keep trying until connected (allows us to handle redirections)
      do
      {
         reply = station_connect();
         check_buttons(); // so user can choose another station
      } while ( reply == 0 );

      Serial.print("Connected to ");
      Serial.println(station[currentstation].name);
      bytecount = metaint;
   }

   // If there's data, handle it in 32 byte chunks, catering for metadata
   if (webclient.available() > 0)
   {
      // Read 32 bytes and throw them directly to the MP3 player
      // (this actualy works as long as nothing too complex happens in loop()!)

      if ( metaint != 0 )
      {
         // We have metadata, so we need to read the appropriate amounts

         if ( bytecount >= 32 )
         {
            // It's okay, we have data to go until metadata block
            bytes = webclient.read(buffer, 32);
            audioplayer.playChunk(buffer, bytes);

            // Subtract bytes from the bytecount
            bytecount -= bytes;
            if (bytecount == 0)
            {
               // If it's reached zero, check to see if there is information to read
               read_icy_metadata();
               bytecount = metaint; // reset byte count for next bit of metadata
            }
         }
         else
         {
            // We have less than 32 bytes before metadata, so read what's left
            bytes = webclient.read(buffer, bytecount);
            audioplayer.playChunk(buffer, bytes);

            // Subtract bytes from the bytecount (should equal zero!)
            bytecount -= bytes;
            if (bytecount == 0)
            {
               // If it's reached zero, check to see if there is information to read
               read_icy_metadata();
               bytecount = metaint; // reset byte count for next bit of metadata
            }
         }
      }
      else
      {
         // There is no metadata, so just read and play
         bytes = webclient.read(buffer, 32);
         audioplayer.playChunk(buffer, bytes);
      }
   }

   // Check the buttons
   check_buttons();

   // LCD to twiddle?
   if ( lcddelay != 0 )
   {
      // Expired?
      if ( lcddelay < millis() )
      {
         // Yes, so restore what should normally be shown
         lcd_title();
         lcddelay = 0;
      }
   }

   // Warning: Adding more to this loop will probably require
   //          some sort of buffering to be implemented...
}    


void mp3_initialise()
{
   // Initialise the VS1053 to play in MP3 mode
   audioplayer.begin();
   if ( audioplayer.isChipConnected() == 0 )
   {
      lcd_output("VS1053 audio IC", "not responding");
      delay(2500); // nothing much we can do here...
   }
   audioplayer.switchToMp3Mode();
   audioplayer.setVolume(volume);
}


void lcd_output(char *lineone, char *linetwo)
{
   // Write the two lines given to the LCD
   
   if ( !lcdpresent )
      return;
      
   lcd.clear();
   lcd.setCursor(0, 0);
   lcd.print(lineone);
   lcd.setCursor(0, 1);
   lcd.print(linetwo);

   return;
}


void wifi_connect()
{
   // Try to connect to the AP
   int  timeout = 0;
   long sigstrength = 0;
   char sigreport[32] = "";
   
   lcd_output("WiFi connecting:", ssid);
   Serial.print("Trying to connect to ");
   Serial.println(ssid);

   // Kill off any previous behaviour
   WiFi.disconnect(true);

   // Start the WiFi system
   WiFi.mode(WIFI_STA);
   WiFi.begin(ssid, pass);
   WiFi.setHostname(hostname);

   // Wait while it connects
   while (WiFi.status() != WL_CONNECTED)
   {
      delay(350);
      Serial.print(".");

      // If it's taken too long, force a restart
      // (this usually works - no idea why)
      timeout += 1;
      if ( timeout > 10 )
      {
         Serial.println("Restarting...");
         lcd_output("Restarting...", "");
         delay(500);
         ESP.restart();
      }
   }

   // Report connected, and WiFi signal strength
   sigstrength = WiFi.RSSI();
   sprintf(sigreport, "Connect %lddBm", sigstrength);
   Serial.println(sigreport);
   lcd_output(sigreport, ssid);
   delay(1500); // delay so message can be seen
}


int station_connect()
{
   // Connect to the current station, returns ZERO if did NOT connect.
   // Try again - host/path may have been updated if redirect.
   //
   String header;
   String newhost;
   char hdrbuf[128] = "";
   int  hdrposn = 0;
   int  twonewlines = 0;
   int  thisbyte = 0;
   int  lastbyte = 0;

   digitalWrite(2, LOW); // turn off blue LED
   lcd_output("Connecting to", station[currentstation].name);
   Serial.print("Connecting to ");
   Serial.print(station[currentstation].host);
   Serial.print(station[currentstation].path);
   sprintf(hdrbuf, " on port %d.", station[currentstation].port);
   Serial.println(hdrbuf);
   
   title[0] = '\0';
   artist[0] = '\0';
   metaint = 0;
   
   // Open a connection
   if ( webclient.connect(station[currentstation].host, station[currentstation].port) )
   {
      Serial.println("Port opened, sending request."); 

      if ( wantmetadata )
      {
         // We want Icy metadata
         webclient.print(String("GET ") + station[currentstation].path + " HTTP/1.1\r\n" +
                         "Host: " + station[currentstation].host + "\r\n" +
                         "Icy-MetaData:1\r\n" +
                         "Connection: close\r\n\r\n");
      }
      else
      {
         // We do NOT want Icy metadata
         webclient.print(String("GET ") + station[currentstation].path + " HTTP/1.1\r\n" +
                         "Host: " + station[currentstation].host + "\r\n" +
                         "Connection: close\r\n\r\n");
      }
      
      // Now we need to read header lines and extract data
      delay(500); // wait for some data to come back
      if (webclient.available() == 0)
      {
         lcd_output(station[currentstation].name, "No data,retrying");
         delay(1500); // 1.5 sec delay before returning failed
         return 0;
      }

      // Keep reading until we read two LF bytes in a row
      while ( !twonewlines )
      {
         // Read a line
         hdrposn = 0;
         hdrbuf[0] = '\0';
         do
         {
            // Read a byte
            thisbyte = webclient.read();
            if ( thisbyte > 31 )
            {
               // If a printable, add it to the buffer
               hdrbuf[hdrposn++] = thisbyte;
               hdrbuf[hdrposn] = '\0';
               lastbyte = thisbyte;

               // If too long, just loop back. It's probably just
               // overlong rubbish like a cookie line or somesuch...
               if ( hdrposn > 127 )
                  hdrposn = 0;
            }
            else
            {
               // It's a control character - is it an LF?
               if ( thisbyte == 10 )
               {
                  // If this byte is LF and so was the last one...
                  if ( lastbyte == thisbyte )
                     twonewlines = 1; // flag we've reached the end of the headers
                  
                  lastbyte = thisbyte;
               }
            }
         } while ( thisbyte != 10 );

         // We have a header line, so let's work out what it is
         Serial.println(hdrbuf);
         header = hdrbuf;

         // Deal with "Location:" header for redirection
         header.toLowerCase();
         if ( header.startsWith("location: http://") )
         {
            String newhdr;
            String newhost;
            String newpath;
            int    newport = 80; // default to HTTP
            int    posn;

            // Work out where we're supposed to point to now
            Serial.print("Redirection -> ");
            header = hdrbuf;
            newhdr = header.substring(17);
            Serial.println(newhdr);

            // Look to split URI into host and path
            posn = newhdr.indexOf("/");
            if ( posn > 0 )
            {
               newpath = newhdr.substring( posn );
               newhost = newhdr.substring( 0, posn );
            }
            // Look to split host into host and port number
            posn = newhdr.indexOf(":");
            if ( posn > 0 )
            {
               newport = newhdr.substring( posn + 1 ).toInt();
               newhost = newhdr.substring( 0, posn );
            }
           
            Serial.println("Specifying new host " +
                           newhost +
                           " at " +
                           newpath);

            strncpy(station[currentstation].host, newhost.c_str(), 128);
            strncpy(station[currentstation].path, newpath.c_str(), 128);
            station[currentstation].port = newport;

            // Don't try closing the connection, it'll already have been done
            // and doing it ourselves will cause the device to hang (great code!)
            Serial.println("About to redirect");
            lcd_output("Redirecting...", "");
            delay(500); // half a second so message can be seen (briefly!)

            return 0;
         }

         // Read proper name of station
         // look for "icy-name:" and take substring(9)
         
         // Read station bitrate
         if ( header.startsWith("icy-br:" ) )
            bitrate = header.substring(7).toInt();

         // Read ICY info interval
         if ( header.startsWith("icy-metaint:" ) )
            metaint = header.substring(12).toInt();
      }

      // If we have a bitrate, write that to the LCD in line two
      if ( bitrate > 0 )
      {
         lcd_title(); // will put bitrate into second line as no title/artist info yet
      }
      else
      {
         // No bitrate given, so output the hostname in the second line
         lcd_output(station[currentstation].name, station[currentstation].host);
      }
   }
   else
   {
      Serial.println("Connection failed.");
      lcd_output(station[currentstation].name, "Connect failed!");
      delay(1500);
      return 0;
   }

   // Assume by now that we're connected
   connected = 1;
   digitalWrite(2, HIGH); // turn on blue LED

   return 1; // connected!
}


void read_icy_metadata()
{
   // Read the metadata, look for a title

   int  mdatlen = 0;
   int  recurse = 0;
   int  corrupt = 0;
   char *start = NULL;
   char *end = NULL;

   // Read length byte
   mdatlen = webclient.read();

   // No data?
   if ( mdatlen == 0 )
      return;

   // The size is actually multiplied by 16, to allow up to 4080 bytes from a single size byte
   // (the payload size does not include the size byte)
   mdatlen = mdatlen * 16;

   // Read the data
   for ( recurse = 0; recurse < mdatlen; recurse++ )
   {
      metadata[recurse] = webclient.read();

      // I don't know if TAB is valid in metadata, but bytes 1-8 aren't, so if
      // we see any of those, assume the data has been corrupted and that we
      // can no longer look for metadata as we're out of sync.
      // (a poor WiFi signal can cause data loss which will trigger this)
      if ( (metadata[recurse] > 0) && (metadata[recurse] < 9) )
         corrupt = 1;
   }
   metadata[mdatlen] = '\0';
   Serial.print("Metadata \"");
   Serial.print(metadata);
   Serial.println("\"");

   // If corrupt, note this and abort
   if ( corrupt )
   {
      // There's no possible recovery, metadata does not have any distinctive
      // header, so just flag us as not connected so we can reconnect.
      lcd_output(station[currentstation].name, "Metadata SyncErr");
      connected = 0;
      digitalWrite(2, LOW); // turn off blue LED

      return;
   }

   // Okay, now let's look for the title and artist names
   // We will see something like:
   //   StreamTitle='Kirsty MacColl - Days';
   // or:
   //   StreamTitle=' - ';
   // The latter being in between songs, during adverts, etc.
   // There may be other metadata, such as "StreamUrl='';".
   // May also be:
   //   StreamTitle='';
   // if the channel doesn't bother with information
   // (but has to support it as the client will be expecting it)
   
   start = strstr(metadata, "StreamTitle");
   if ( start != NULL )
   {
      // We have found the streamtitle
      start += 12; // skip over "StreamTitle="

      // Now look for the end marker
      end = strstr(start, ";");
      if ( end == NULL )
      {
         // No end, so just set it as the string length
         end = (char *)start + strlen(start);
      }

      // Quoted string?
      if ( start[0] == '\'')
      {
         // It seems as if quotes may be optional, so handle them.
         start += 1;
         end -= 1;
      }

      // Terminull the part we want
      end[0] = '\0';

      // Now start should point to a string like:
      //   "Bananarama - Venus"
      // Or maybe just " - ", or even just "".

      // Trap empty metadata
      if ( strcmp(start, " - ") == 0 )
      {
         // No metadata right now, so display the station name
         title[0] = '\0';
         artist[0] = '\0';
         lcd_title();
         return;
      }

      // Okay, sort out what's the title and what's the artist
      end = strstr(start, " - ");
      if ( end == NULL )
      {
         // Separator not found, output station title and this string
         title[0] = '\0';
         strncpy(artist, start, 16);
         artist[16] = '\0';
         lcd_title();
      }
      else
      {
         // First part is artist, second part is title
         end[0] = '\0'; // Terminate artist part
         end += 3; // Skip to start of title
         strncpy(title, end, 16);
         title[16] = '\0';
         strncpy(artist, start, 16);
         artist[16] = '\0';
         lcd_title();

         // TODO:
         // As I write this JPopsuki has just given me:
         //   StreamTitle='キャプテンストライダム - バースデー';
         // Which on the LCD looks like:
         //   ɛ  ɛ シɛ ケɛ  ɛ シ 
         //   ɛ ュɛ 」ɛ  ɛ  ɛ ウɛ
         // So I think in the future it might be useful
         // to detect UTF-8 and copy across as '?' or
         // something for the things that can't be shown.
         //
         // LCD character set (Hitachi HD44780):
         //   !"#$%&'()*+,-./
         //  0123456789:;<=>?     <-- this is like ASCII
         //  @ABCDEFGHIJKLMNO
         //  PQRSTUVWXYZ[¥]^_     <-- but note the '¥' instead of '\'
         //  `abcdefghijklmno
         //  pqrstuvwxyz{|}←→    <-- arrows instead of ~ and unprintable
         //  [ not defined ]
         //  [ not defined ]
         //   。「」、・ヲァィゥェォャュョッ     <-- small katakana
         //  ㆒アイウエオカキクケコサシスセソ     <-- katakana
         //  タチツテトナニヌネノハヒフヘホマ
         //  ミムメモヤユヨラリルレロワン゛゜
         //  αäßɛµσρℊ√₁j̽¢Lñö     <-- more or less, greek/maths
         //  ρϕθ∞ΩüΣπ※У千万円÷ ▊     <-- more or less, greek/maths
         //
      }      
   }
}


void lcd_title()
{
   // Output something on the LCD to reflect title, or failing
   // that, the station.
   char report[32] = "";

   if ( strlen(title) == 0 )
   {
      // We have no title

      if ( strlen(artist) == 0 )
      {
         // We have no artist either, so output station info
         sprintf(report, "%dkbit MP3", bitrate);
         lcd_output(station[currentstation].name, report);
      }
      else
      {
         // We have an artist but no title (probably omething odd in StreamInfo)
         // so display station name and whatever is in artist.
         lcd_output(station[currentstation].name, artist);      
      }
   }
   else
   {
      // We have a title (and thus presumably an artist) so display it
      lcd_output(title, artist);
   }
}


void check_buttons()
{
   // Check the buttons and perform actions on button release (unless BOTH pressed)
   
   bool level = 0;
   unsigned long ticker = 0;
   int  thisbutton = 0;
   char report[32];
   char volbar[17];
   long sigstrength = 0;

   // Deal with the buttons
   for ( thisbutton = 0; thisbutton < 2; thisbutton++ )
   {
      level = (digitalRead( (PREVBUTTON + thisbutton) ) == HIGH );
      if ( level != button[thisbutton].state )
      {
         if ( !level )
         {
            // HIGH to LOW - button press
            button[thisbutton].press = millis();
            button[thisbutton].state = level;

            // Detect if the other button has been pressed as well
            if ( ( button[thisbutton].state == LOW ) &&
                 ( button[(thisbutton ^ 1)].state == LOW ) )
            {
               // Both buttons pressed - toggle whether or not we want metadata
               wantmetadata = wantmetadata ^ 1;
               sigstrength = WiFi.RSSI();
               sprintf(report, "WiFi %lddBm", sigstrength);
               if ( wantmetadata == 1 )
                  lcd_output("MetadataEnabled", report);
               else
                  lcd_output("MetadataDisabled", report);
               connected = 0;
               digitalWrite(2, LOW); // turn off blue LED
               button[0].state = HIGH;
               button[1].state = HIGH;
               delay(1000);
               return;
            }
         }
         else
         {
           // LOW to HIGH - button release
           button[thisbutton].release = millis();
           button[thisbutton].state = level;

           // Is there an action to perform?
           ticker = button[thisbutton].release - button[thisbutton].press;

           if ( ticker > 2500 )
           {
              // Pressed for longer than two and a half seconds, display some info
              if ( thisbutton == 1)
              {
                 // +ve button, report WiFi status
                 sigstrength = WiFi.RSSI();
                 sprintf(report, "WiFi is %lddBm", sigstrength);
                 if (   sigstrength >  -30 )                           strcpy(volbar, "(perfect)");
                 if ( ( sigstrength >= -50 ) && ( sigstrength < -30) ) strcpy(volbar, "(excellent)");
                 if ( ( sigstrength >= -60 ) && ( sigstrength < -50) ) strcpy(volbar, "(good)");
                 if ( ( sigstrength >= -70 ) && ( sigstrength < -60) ) strcpy(volbar, "(acceptable)");
                 if ( ( sigstrength >= -80 ) && ( sigstrength < -70) ) strcpy(volbar, "(poor)");
                 if ( ( sigstrength >= -90 ) && ( sigstrength < -80) ) strcpy(volbar, "(very poor)");
                 lcd_output(report, volbar);
                 lcddelay = millis() + 3000; // wait for longer before going away (3 sec)
              }
              else
              {
                 // -ve button, report system status
                 // 1234567890123456
                 // Core x @ 123MHz
                 // MemFree: 123456
                 sprintf(report, "Core %d @ %dMHz", xPortGetCoreID(), ESP.getCpuFreqMHz());
                 sprintf(volbar, "MemFree: %d", ESP.getFreeHeap());
                 lcd_output(report, volbar);
                 lcddelay = millis() + 3000; // wait for longer before going away (3 sec)
              }
              return;
           }
           
           if ( ticker > 500 )
           {
              // Pressed for longer than half a second, it is a long press
              if ( thisbutton == 1 )
              {
                 // Next station
                 currentstation += 1;
                 if ( currentstation == stationcount )
                    currentstation = 0; // wrap around
              }
              else
              {
                 // Previous station
                 currentstation -= 1;
                 if ( currentstation < 0 )
                    currentstation = (stationcount - 1); // wrap around inverse
              }
              sprintf(report, "Station change to %d", currentstation);
              Serial.println(report);
              connected = 0; // to force connecting to new station
              digitalWrite(2, LOW); // turn off blue LED
           }
           else
           {
              if ( ticker > 50 )
              {
                 // Held for more than 5cs, it is a regular press
                 if ( thisbutton == 1 )
                 {
                    // Increase volume
                    volume += 3;
                    if ( volume > 98 )
                       volume = 98; // clip at 98
                 }
                 else
                 {
                    // Decrease volume
                    volume -= 3;
                    if ( volume < 0 )
                       volume = 0;
                 }
                 audioplayer.setVolume(volume);

                 // Construct the LCD report with volume bar
                 sprintf(report, "Volume %d", volume);
                 Serial.println(report);
                 for (int recurse = 0; recurse < 16; recurse++)
                    volbar[recurse] = ' ';
                 volbar[16] = '\0';
                 for (int recurse = 0; recurse < (volume / 6.25); recurse++)
                    volbar[recurse] = 255;
                 volbar[16] = '\0';
                 lcd_output(report, volbar);
                 lcddelay = millis() + 1500; // go away after one and a half seconds
              } // if ticker > 50 block
           } // if ticker > 500 block
         } // high to low or low to high block
      } // if level different to button block
   } // for thisbutton ... loop

}
