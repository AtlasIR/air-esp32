/*********
Rui Santos
Complete project details at https://randomnerdtutorials.com
*********/
// Import required libraries
#include "WiFi.h"
#include "ESPAsyncWebServer.h"
#include "SPIFFS.h"
#include "fauxmoESP.h"

#define SERIAL_BAUDRATE 115200
#define disc_26 "wow"
#define disc_27 "tx"
#define disc_25 "ops"
#define disc_33 "mx"
#define disc_21 "ac fail"
#define disc_22 "power enable"
#define disc_23 "diag reset"
#define disc_32 "transmit mute"
#define disc_13 "power"

fauxmoESP fauxmo;

// Replace with your network credentials
const char* ssid = "SGAMFGuest";
const char* password = "belowzero";
//#define WIFI_SSID "SGAMFGuest"
//#define WIFI_PASS "belowzero"

// Set Discrete GPIO
const int output26 = 26;
const int output27 = 27;
const int output25 = 25;
const int output33 = 33;
const int output21 = 21;
const int output22 = 22;
const int output23 = 23;
const int output32 = 32;
const int output13 = 13;

 String z1 = " ";
 String z2 = " ";
// Stores Discrete state
String output26State = "off";
String output27State = "off";
String output25State = "off";
String output33State = "off";
String output21State = "off";
String output22State = "off";
String output23State = "off";
String output32State = "off";
String output13State = "off";


// Create AsyncWebServer object on port 80
AsyncWebServer server(90);
// Replaces placeholder with LED state value
String processor(const String& var){
Serial.println(var);

if(var == "STATE"){
if (digitalRead(output26)){
output26State = "ON";
}
else{
output26State = "OFF";
}
Serial.print(output26State);
return output26State;
if (digitalRead(output27)){
output27State = "ON";
}
else{
output27State = "OFF";
}
Serial.print(output27State);
return output27State;
if (digitalRead(output25)){
output25State = "ON";
}
else{
output25State = "OFF";
}
Serial.print(output25State);
return output25State;
if (digitalRead(output33)){
output33State = "ON";
}
else{
output33State = "OFF";
}
Serial.print(output33State);
return output33State;
if (digitalRead(output21)){
output21State = "ON";
}
else{
output21State = "OFF";
}
Serial.print(output21State);
return output21State;
if (digitalRead(output22)){
output22State = "ON";
}
else{
output22State = "OFF";
}
Serial.print(output22State);
return output22State;
if (digitalRead(output23)){
output23State = "ON";
}
else{
output23State = "OFF";
}
Serial.print(output23State);
return output23State;
if (digitalRead(output32)){
output32State = "ON";
}
else{
output32State = "OFF";
}
Serial.print(output32State);
return output32State;
if (digitalRead(output13)){
output13State = "ON";
}
else{
output13State = "OFF";
}
Serial.print(output13State);
return output13State;
return String();
}
}

void setup(){
// Serial port for debugging purposes
Serial.begin(SERIAL_BAUDRATE);
 Serial.println("Connecting to ");
  Serial.println(ssid);

  //connect to your local wi-fi network
  WiFi.begin(ssid, password);

  //check wi-fi is connected to wi-fi network
  while (WiFi.status() != WL_CONNECTED) {
  delay(1000);
  Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected..!");
  Serial.print("Got IP: ");  Serial.println(WiFi.localIP());
//Serial.println("This demo expects 3 pieces of data - text, an integer and a floating point value");
//    Serial.println("Enter data in this style <############>  ");
//    Serial.println();

// Initialize the output variables as outputs
pinMode(output26, OUTPUT);
pinMode(output27, OUTPUT);
pinMode(output25, OUTPUT);
pinMode(output33, OUTPUT);
pinMode(output21, OUTPUT);
pinMode(output22, OUTPUT);
pinMode(output23, OUTPUT);
pinMode(output32, OUTPUT);
pinMode(output13, OUTPUT);
// Set outputs to High
digitalWrite(output26, HIGH);
digitalWrite(output27, HIGH);
digitalWrite(output25, HIGH);
digitalWrite(output33, HIGH);
digitalWrite(output21, HIGH);
digitalWrite(output22, HIGH);
digitalWrite(output23, HIGH);
digitalWrite(output32, HIGH);
digitalWrite(output13, HIGH);
// Initialize SPIFFS
if(!SPIFFS.begin(true)){
Serial.println("An Error has occurred while mounting SPIFFS");
return;
}

//Serial.print("Setting AP (Access Point)…");
//// Remove the password parameter, if you want the AP (Access Point) to be open
//WiFi.softAP(ssid, password);
//IPAddress IP = WiFi.softAPIP();
//Serial.print("AP IP address: ");
//Serial.println(IP);
//server.begin();

server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to load style.css file
server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
request->send(SPIFFS, "/style.css", "text/css");
});
// Route to set GPIO to HIGH
server.on("/26/on", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output26, LOW);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to LOW
server.on("/26/off", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output26, HIGH);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to HIGH
server.on("/27/on", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output27, LOW);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to LOW
server.on("/27/off", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output27, HIGH);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to HIGH
server.on("/25/on", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output25, LOW);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to LOW
server.on("/25/off", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output25, HIGH);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to HIGH
server.on("/33/on", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output33, LOW);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to LOW
server.on("/33/off", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output33, HIGH);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to HIGH
server.on("/21/on", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output21, LOW);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to LOW
server.on("/21/off", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output21, HIGH);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to HIGH
server.on("/22/on", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output22, LOW);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to LOW
server.on("/22/off", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output22, HIGH);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to HIGH
server.on("/23/on", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output23, LOW);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to LOW
server.on("/23/off", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output23, HIGH);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to HIGH
server.on("/32/on", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output32, LOW);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to LOW
server.on("/32/off", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output32, HIGH);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// Route to set GPIO to HIGH
server.on("/13/on", HTTP_GET, [](AsyncWebServerRequest *request){
digitalWrite(output13, LOW);
request->send(SPIFFS, "/index.html", String(), false, processor);
});
// By default, fauxmoESP creates it's own webserver on the defined port
  // The TCP port must be 80 for gen3 devices (default is 1901)
  // This has to be done before the call to enable()
  fauxmo.createServer(true); // not needed, this is the default value
  fauxmo.setPort(80); // This is required for gen3 devices

  // You have to call enable(true) once you have a WiFi connection
  // You can enable or disable the library at any moment
  // Disabling it will prevent the devices from being discovered and switched
  fauxmo.enable(true);
  // You can use different ways to invoke alexa to modify the devices state:
  // "Alexa, turn lamp two on"

  // Add virtual devices
  fauxmo.addDevice(disc_26);
  fauxmo.addDevice(disc_27);
  fauxmo.addDevice(disc_25);
  fauxmo.addDevice(disc_33);
  fauxmo.addDevice(disc_21);
  fauxmo.addDevice(disc_22);
  fauxmo.addDevice(disc_23);
  fauxmo.addDevice(disc_32);
  fauxmo.addDevice(disc_13);
  
  fauxmo.onSetState([](unsigned char device_id, const char * device_name, bool state, unsigned char value) {
    // Callback when a command from Alexa is received. 
    // You can use device_id or device_name to choose the element to perform an action onto (relay, LED,...)
    // State is a boolean (ON/OFF) and value a number from 0 to 255 (if you say "set kitchen light to 50%" you will receive a 128 here).
    // Just remember not to delay too much here, this is a callback, exit as soon as possible.
    // If you have to do something more involved here set a flag and process it in your main loop.
        
    Serial.printf("[MAIN] Device #%d (%s) state: %s value: %d\n", device_id, device_name, state ? "ON" : "OFF", value);
    if ( (strcmp(device_name, disc_26) == 0) ) {
      // this just sets a variable that the main loop() does something about
      Serial.println("Wow switched by Alexa");
      }
  });
}
void loop() {
  // fauxmoESP uses an async TCP server but a sync UDP server
  // Therefore, we have to manually poll for UDP packets
  fauxmo.handle();

}




//// Start server
//server.begin();
//char example[]={"D030045055080"};
//int z1    = valueFromString( example, 1, 3);
//int z2   = valueFromString( example, 4, 3);
//int z3     = valueFromString ( example, 7, 3);
//int z4   = valueFromString ( example, 10, 3);
//
//
//Serial.print( "Zone 1 ");
//Serial.println (z1);
//Serial.print( "Zone 2 ");
//Serial.println (z2);
//Serial.print( "Zone 3 ");
//Serial.println (z3);
//Serial.print( "Zone 4 ");
//Serial.println (z4);
//
//}
//
//void loop()
//{
//}
//int valueFromString(char* string,int start, int width)
//{
//int value=0;
//for( int n=0; n < width; n++ )
//  value = value * 10 + string[start +n] - '0';
//
//return value; 
//}
