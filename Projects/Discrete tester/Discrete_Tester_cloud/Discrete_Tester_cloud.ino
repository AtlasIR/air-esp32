/*
   -- DISCRETE BOARD TESTER --
   
   This source code of graphical user interface 
   has been generated automatically by RemoteXY editor.
   To compile this code using RemoteXY library 2.4.3 or later version 
   download by link http://remotexy.com/en/library/
   To connect using RemoteXY mobile app by link http://remotexy.com/en/download/                   
     - for ANDROID 4.5.1 or later version;
     - for iOS 1.4.1 or later version;
    
   This source code is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.    
*/

//////////////////////////////////////////////
//        RemoteXY include library          //
//////////////////////////////////////////////

// RemoteXY select connection mode and include library 
#define REMOTEXY_MODE__ESP32CORE_WIFI_CLOUD
#include <WiFi.h>

#include <RemoteXY.h>

// RemoteXY connection settings 
#define REMOTEXY_WIFI_SSID "SGAMFGuest"
#define REMOTEXY_WIFI_PASSWORD "belowzero"
#define REMOTEXY_CLOUD_SERVER "cloud.remotexy.com"
#define REMOTEXY_CLOUD_PORT 6376
#define REMOTEXY_CLOUD_TOKEN "706c34ac04a2e610959722b94554cc50"
long randNumber;

// RemoteXY configurate  
#pragma pack(push, 1)
uint8_t RemoteXY_CONF[] =
  { 255,9,0,16,0,180,1,10,163,6,
  131,1,6,1,20,7,3,1,20,7,
  1,2,31,80,97,103,101,0,131,0,
  29,1,20,7,39,1,20,7,2,2,
  31,80,97,103,101,32,49,0,68,52,
  3,28,95,33,3,55,58,42,0,8,
  36,135,94,204,90,79,78,69,32,49,
  0,90,79,78,69,32,50,0,90,79,
  78,69,32,51,0,90,79,78,69,32,
  52,0,129,0,8,14,8,3,12,22,
  8,3,1,24,87,79,87,0,129,0,
  34,14,4,3,13,39,8,3,1,24,
  77,88,0,129,0,57,14,6,3,37,
  22,8,3,1,24,79,80,83,0,129,
  0,81,14,6,3,37,39,8,3,1,
  24,77,84,88,0,2,1,3,17,18,
  8,7,26,18,8,2,2,26,31,31,
  79,78,0,79,70,70,0,2,1,27,
  17,18,8,7,42,18,8,2,2,26,
  31,31,79,78,0,79,70,70,0,2,
  1,51,17,18,8,32,26,18,8,2,
  2,26,31,31,79,78,0,79,70,70,
  0,2,1,75,17,18,8,32,42,18,
  8,2,2,26,31,31,79,78,0,79,
  70,70,0,2,1,79,1,18,8,32,
  11,18,8,0,2,26,31,31,79,78,
  0,79,70,70,0,129,0,61,3,15,
  4,4,12,18,6,0,1,80,79,87,
  69,82,0,129,0,6,13,11,4,11,
  38,10,4,2,8,76,97,98,101,108,
  0,129,0,31,13,11,4,35,38,11,
  4,2,8,76,97,98,101,108,0,129,
  0,54,13,11,4,36,21,11,4,2,
  8,76,97,98,101,108,0,129,0,78,
  13,11,4,11,21,11,4,2,8,76,
  97,98,101,108,0,2,1,3,17,18,
  8,7,26,18,8,1,2,26,31,31,
  79,78,0,79,70,70,0,2,1,27,
  17,18,8,7,42,18,8,1,2,26,
  31,31,79,78,0,79,70,70,0,2,
  1,51,17,18,8,32,26,18,8,1,
  2,26,31,31,79,78,0,79,70,70,
  0,2,1,75,17,18,8,32,42,18,
  8,1,2,26,31,31,79,78,0,79,
  70,70,0 };

  
// this structure defines all the variables and events of your control interface 
struct {

    // input variables
  uint8_t switch_21; // =0 if switch ON and =1 if OFF 
  uint8_t switch_22; // =0 if switch ON and =1 if OFF 
  uint8_t switch_23; // =0 if switch ON and =1 if OFF 
  uint8_t switch_32; // =0 if switch ON and =1 if OFF 
  uint8_t switch_5; // =0 if switch ON and =1 if OFF 
  uint8_t switch_26; // =0 if switch ON and =1 if OFF 
  uint8_t switch_27; // =0 if switch ON and =1 if OFF 
  uint8_t switch_25; // =0 if switch ON and =1 if OFF 
  uint8_t switch_33; // =0 if switch ON and =1 if OFF 

    // output variables
  float onlineGraph_1_var1;
  float onlineGraph_1_var2;
  float onlineGraph_1_var3;
  float onlineGraph_1_var4;

    // other variable
  uint8_t connect_flag;  // =1 if wire connected, else =0 

} RemoteXY;
#pragma pack(pop)

/////////////////////////////////////////////
//           END RemoteXY include          //
/////////////////////////////////////////////

#define PIN_SWITCH_21 21
#define PIN_SWITCH_22 22
#define PIN_SWITCH_23 23
#define PIN_SWITCH_32 32
#define PIN_SWITCH_5 13
#define PIN_SWITCH_26 26
#define PIN_SWITCH_27 27
#define PIN_SWITCH_25 25
#define PIN_SWITCH_33 33


void setup() 
{
  RemoteXY_Init (); 
  
  pinMode (PIN_SWITCH_21, OUTPUT);
  pinMode (PIN_SWITCH_22, OUTPUT);
  pinMode (PIN_SWITCH_23, OUTPUT);
  pinMode (PIN_SWITCH_32, OUTPUT);
  pinMode (PIN_SWITCH_5, OUTPUT);
  pinMode (PIN_SWITCH_26, OUTPUT);
  pinMode (PIN_SWITCH_27, OUTPUT);
  pinMode (PIN_SWITCH_25, OUTPUT);
  pinMode (PIN_SWITCH_33, OUTPUT);
  
  // TODO you setup code
  randomSeed(analogRead(5));
  

}

void loop() 
{ 
  RemoteXY_Handler ();
  
  digitalWrite(PIN_SWITCH_21, (RemoteXY.switch_21==0)?HIGH:LOW);
  digitalWrite(PIN_SWITCH_22, (RemoteXY.switch_22==0)?HIGH:LOW);
  digitalWrite(PIN_SWITCH_23, (RemoteXY.switch_23==0)?HIGH:LOW);
  digitalWrite(PIN_SWITCH_32, (RemoteXY.switch_32==0)?HIGH:LOW);
  digitalWrite(PIN_SWITCH_5, (RemoteXY.switch_5==0)?HIGH:LOW);
  digitalWrite(PIN_SWITCH_26, (RemoteXY.switch_26==0)?HIGH:LOW);
  digitalWrite(PIN_SWITCH_27, (RemoteXY.switch_27==0)?HIGH:LOW);
  digitalWrite(PIN_SWITCH_25, (RemoteXY.switch_25==0)?HIGH:LOW);
  digitalWrite(PIN_SWITCH_33, (RemoteXY.switch_33==0)?HIGH:LOW);
  
  // TODO you loop code
  // use the RemoteXY structure for data transfer
  // do not call delay() 
  randNumber = random(10);
  
    RemoteXY.onlineGraph_1_var1 = randNumber;
     randNumber = random(10);
    RemoteXY.onlineGraph_1_var2 = randNumber;
     randNumber = random(10);
    RemoteXY.onlineGraph_1_var3 = randNumber;
     randNumber = random(10);
    RemoteXY.onlineGraph_1_var4 = randNumber; 
}
