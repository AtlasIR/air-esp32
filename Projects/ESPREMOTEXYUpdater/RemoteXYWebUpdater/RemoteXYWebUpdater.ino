#define REMOTEXY_MODE__ESP32CORE_WIFI
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <Update.h>

#include <RemoteXY.h>
const char* host = "esp32";
//const char* ssid = "SGAMFGuest";
//const char* password = "belowzero";
// RemoteXY connection settings 
#define REMOTEXY_WIFI_SSID "JOES2 6760"
#define REMOTEXY_WIFI_PASSWORD "imaguest!"
#define REMOTEXY_SERVER_PORT 6377

WebServer server(80);
long randNumber;
//IPAddress ip;                    // the IP address of your shield
/* Style */
String style =
"<style>#file-input,input{width:100%;height:44px;border-radius:4px;margin:10px auto;font-size:15px}"
"input{background:#f1f1f1;border:0;padding:0 15px}body{background:#3498db;font-family:sans-serif;font-size:14px;color:#777}"
"#file-input{padding:0;border:1px solid #ddd;line-height:44px;text-align:left;display:block;cursor:pointer}"
"#bar,#prgbar{background-color:#f1f1f1;border-radius:10px}#bar{background-color:#3498db;width:0%;height:10px}"
"form{background:#fff;max-width:258px;margin:75px auto;padding:30px;border-radius:5px;text-align:center}"
".btn{background:#3498db;color:#fff;cursor:pointer}</style>";

/* Login page */
String loginIndex = 
"<form name=loginForm>"
"<h1>ESP32 Login</h1>"
"<input name=userid placeholder='User ID'> "
"<input name=pwd placeholder=Password type=Password> "
"<input type=submit onclick=check(this.form) class=btn value=Login></form>"
"<script>"
"function check(form) {"
"if(form.userid.value=='admin' && form.pwd.value=='admin')"
"{window.open('/serverIndex')}"
"else"
"{alert('Error Password or Username')}"
"}"
"</script>" + style;
 
/* Server Index Page */
String serverIndex = 
"<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
"<form method='POST' action='#' enctype='multipart/form-data' id='upload_form'>"
"<input type='file' name='update' id='file' onchange='sub(this)' style=display:none>"
"<label id='file-input' for='file'>   Choose file...</label>"
"<input type='submit' class=btn value='Update'>"
"<br><br>"
"<div id='prg'></div>"
"<br><div id='prgbar'><div id='bar'></div></div><br></form>"
"<script>"
"function sub(obj){"
"var fileName = obj.value.split('\\\\');"
"document.getElementById('file-input').innerHTML = '   '+ fileName[fileName.length-1];"
"};"
"$('form').submit(function(e){"
"e.preventDefault();"
"var form = $('#upload_form')[0];"
"var data = new FormData(form);"
"$.ajax({"
"url: '/update',"
"type: 'POST',"
"data: data,"
"contentType: false,"
"processData:false,"
"xhr: function() {"
"var xhr = new window.XMLHttpRequest();"
"xhr.upload.addEventListener('progress', function(evt) {"
"if (evt.lengthComputable) {"
"var per = evt.loaded / evt.total;"
"$('#prg').html('progress: ' + Math.round(per*100) + '%');"
"$('#bar').css('width',Math.round(per*100) + '%');"
"}"
"}, false);"
"return xhr;"
"},"
"success:function(d, s) {"
"console.log('success!') "
"},"
"error: function (a, b, c) {"
"}"
"});"
"});"
"</script>" + style;

// RemoteXY configurate  
#pragma pack(push, 1)
uint8_t RemoteXY_CONF[] =
  { 255,9,0,16,0,180,1,10,163,6,
  131,1,6,1,20,7,3,1,20,7,
  1,2,31,80,97,103,101,0,131,0,
  29,1,20,7,39,1,20,7,2,2,
  31,80,97,103,101,32,49,0,68,52,
  3,28,95,33,3,55,58,42,0,8,
  36,135,94,204,90,79,78,69,32,49,
  0,90,79,78,69,32,50,0,90,79,
  78,69,32,51,0,90,79,78,69,32,
  52,0,129,0,8,14,8,3,12,22,
  8,3,1,24,87,79,87,0,129,0,
  34,14,4,3,13,39,8,3,1,24,
  77,88,0,129,0,57,14,6,3,37,
  22,8,3,1,24,79,80,83,0,129,
  0,81,14,6,3,37,39,8,3,1,
  24,77,84,88,0,2,1,3,17,18,
  8,7,26,18,8,2,2,26,31,31,
  79,78,0,79,70,70,0,2,1,27,
  17,18,8,7,42,18,8,2,2,26,
  31,31,79,78,0,79,70,70,0,2,
  1,51,17,18,8,32,26,18,8,2,
  2,26,31,31,79,78,0,79,70,70,
  0,2,1,75,17,18,8,32,42,18,
  8,2,2,26,31,31,79,78,0,79,
  70,70,0,2,1,79,1,18,8,32,
  11,18,8,0,2,26,31,31,79,78,
  0,79,70,70,0,129,0,61,3,15,
  4,4,12,18,6,0,1,80,79,87,
  69,82,0,129,0,6,13,11,4,11,
  38,10,4,2,8,76,97,98,101,108,
  0,129,0,31,13,11,4,35,38,11,
  4,2,8,76,97,98,101,108,0,129,
  0,54,13,11,4,36,21,11,4,2,
  8,76,97,98,101,108,0,129,0,78,
  13,11,4,11,21,11,4,2,8,76,
  97,98,101,108,0,2,1,3,17,18,
  8,7,26,18,8,1,2,26,31,31,
  79,78,0,79,70,70,0,2,1,27,
  17,18,8,7,42,18,8,1,2,26,
  31,31,79,78,0,79,70,70,0,2,
  1,51,17,18,8,32,26,18,8,1,
  2,26,31,31,79,78,0,79,70,70,
  0,2,1,75,17,18,8,32,42,18,
  8,1,2,26,31,31,79,78,0,79,
  70,70,0 };
  
// this structure defines all the variables and events of your control interface 
struct {

    // input variables
  uint8_t switch_21; // =0 if switch ON and =1 if OFF 
  uint8_t switch_22; // =0 if switch ON and =1 if OFF 
  uint8_t switch_23; // =0 if switch ON and =1 if OFF 
  uint8_t switch_32; // =0 if switch ON and =1 if OFF 
  uint8_t switch_5; // =0 if switch ON and =1 if OFF 
  uint8_t switch_26; // =0 if switch ON and =1 if OFF 
  uint8_t switch_27; // =0 if switch ON and =1 if OFF 
  uint8_t switch_25; // =0 if switch ON and =1 if OFF 
  uint8_t switch_33; // =0 if switch ON and =1 if OFF 

    // output variables
  float onlineGraph_1_var1;
  float onlineGraph_1_var2;
  float onlineGraph_1_var3;
  float onlineGraph_1_var4;

    // other variable
  uint8_t connect_flag;  // =1 if wire connected, else =0 

} RemoteXY;
#pragma pack(pop)

/////////////////////////////////////////////
//           END RemoteXY include          //
/////////////////////////////////////////////

#define PIN_SWITCH_21 21
#define PIN_SWITCH_22 22
#define PIN_SWITCH_23 23
#define PIN_SWITCH_32 32
#define PIN_SWITCH_5 13
#define PIN_SWITCH_26 26
#define PIN_SWITCH_27 27
#define PIN_SWITCH_25 25
#define PIN_SWITCH_33 33


/* setup function */
void setup() {
  Serial.begin(115200);
  
RemoteXY_Init (); 
////print the local IP address
//  ip = WiFi.localIP();
//  Serial.println(ip);
  Serial.println(REMOTEXY_WIFI_SSID);
  /*use mdns for host name resolution*/
  if (!MDNS.begin(host)) { //http://esp32.local
    Serial.println("Error setting up MDNS responder!");
    while (1) {
      delay(1000);
    }
  }
  Serial.println("mDNS responder started");
  /*return index page which is stored in serverIndex */
  server.on("/", HTTP_GET, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/html", loginIndex);
  });
  server.on("/serverIndex", HTTP_GET, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/html", serverIndex);
  });
  /*handling uploading firmware file */
  server.on("/update", HTTP_POST, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/plain", (Update.hasError()) ? "FAIL" : "OK");
    ESP.restart();
  }, []() {
    HTTPUpload& upload = server.upload();
    if (upload.status == UPLOAD_FILE_START) {
      Serial.printf("Update: %s\n", upload.filename.c_str());
      if (!Update.begin(UPDATE_SIZE_UNKNOWN)) { //start with max available size
        Update.printError(Serial);
      }
    } else if (upload.status == UPLOAD_FILE_WRITE) {
      /* flashing firmware to ESP*/
      if (Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
        Update.printError(Serial);
      }
    } else if (upload.status == UPLOAD_FILE_END) {
      if (Update.end(true)) { //true to set the size to the current progress
        Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
      } else {
        Update.printError(Serial);
      }
    }
  });
  server.begin();

  
  
  pinMode (PIN_SWITCH_21, OUTPUT);
  pinMode (PIN_SWITCH_22, OUTPUT);
  pinMode (PIN_SWITCH_23, OUTPUT);
  pinMode (PIN_SWITCH_32, OUTPUT);
  pinMode (PIN_SWITCH_5, OUTPUT);
  pinMode (PIN_SWITCH_26, OUTPUT);
  pinMode (PIN_SWITCH_27, OUTPUT);
  pinMode (PIN_SWITCH_25, OUTPUT);
  pinMode (PIN_SWITCH_33, OUTPUT);
  
  // TODO you setup code
  randomSeed(analogRead(5));
}

void loop(void) {
  server.handleClient();
RemoteXY_Handler ();
  
  digitalWrite(PIN_SWITCH_21, (RemoteXY.switch_21==0)?HIGH:LOW);
  digitalWrite(PIN_SWITCH_22, (RemoteXY.switch_22==0)?HIGH:LOW);
  digitalWrite(PIN_SWITCH_23, (RemoteXY.switch_23==0)?HIGH:LOW);
  digitalWrite(PIN_SWITCH_32, (RemoteXY.switch_32==0)?HIGH:LOW);
  digitalWrite(PIN_SWITCH_5, (RemoteXY.switch_5==0)?HIGH:LOW);
  digitalWrite(PIN_SWITCH_26, (RemoteXY.switch_26==0)?HIGH:LOW);
  digitalWrite(PIN_SWITCH_27, (RemoteXY.switch_27==0)?HIGH:LOW);
  digitalWrite(PIN_SWITCH_25, (RemoteXY.switch_25==0)?HIGH:LOW);
  digitalWrite(PIN_SWITCH_33, (RemoteXY.switch_33==0)?HIGH:LOW);
  
  // TODO you loop code
  // use the RemoteXY structure for data transfer
  // do not call delay() 
  randNumber = random(10);
  
    RemoteXY.onlineGraph_1_var1 = randNumber;
     randNumber = random(10);
    RemoteXY.onlineGraph_1_var2 = randNumber;
     randNumber = random(10);
    RemoteXY.onlineGraph_1_var3 = randNumber;
     randNumber = random(10);
    RemoteXY.onlineGraph_1_var4 = randNumber; 
  
}
